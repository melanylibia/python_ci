"""
Unit testing of the automatic batch processing application
"""

import unittest
from  src.app import add

class AppTests(unittest.TestCase):
    def test_app(self):
        """Simple Test"""
        self.assertEqual(add(5), 10)
        self.assertNotEqual(add(150),120)
    def test_error(self):
        """i numeric?"""
        with self.assertRaises(TypeError):
            add("bar")
def suite():
    _suite=unittest.TestSuite()
    _suite.addTest(AppTests("test_app"))
    _suite.addTest(AppTests("test_error"))
    return _suite
if __name__ == "__main__":
    runner = unittest.TextTestRunner()
    runner.run(suite())



        